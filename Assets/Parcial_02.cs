﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Parcial_02 : MonoBehaviour {
    public Material [] materials;
    Renderer renderer;
    float ambient, smooth;
    int zoom, normal;
    Text shade, luz, colorz, texx, tiempo;
    GameObject texto1, texto2, texto3, texto4, texto5, lightPoint, camera;
    Light myLight;

    void Start()
    {
        lightPoint = GameObject.Find("Directional Light");
        myLight = lightPoint.GetComponent<Light>();
        myLight.enabled = !myLight.enabled;

        renderer = GetComponent<Renderer>();
        renderer.material.shader = Shader.Find("Custom/DiffuseShader");
        ambient = 10.0f;
        renderer.material.SetFloat("_MySlider", 10.0f);
        
        renderer.material = materials[0];
        camera = GameObject.Find("Main Camera");
        zoom = 1;
        
        smooth = 0.055f;
        texto1 = GameObject.Find("Shader");
        texto2 = GameObject.Find("Light");
        texto3 = GameObject.Find("Color");
        texto4 = GameObject.Find("Texture");
        texto5 = GameObject.Find("Time");
        tiempo = texto5.GetComponent<Text>();
        shade = texto1.GetComponent<Text>();
        luz = texto2.GetComponent<Text>();
        colorz = texto3.GetComponent<Text>();
        texx = texto4.GetComponent<Text>();
        shade.text = "Shader: Diffuse"; // renderer.material.shader.name;
        colorz.text = "Color: " + renderer.material.color.ToString();
        texx.text = "Texture: OFF";
        /*int alpha = (int)renderer.material.color.a;
        int r = (int)((alpha / 255) * renderer.material.color.r + (1 - alpha / 255) * new Color32(0, 0, 0, 255).r);
        int g = (int)((alpha / 255) * renderer.material.color.g + (1 - alpha / 255) * new Color32(0, 0, 0, 255).g);
        int b = (int)((alpha / 255) * renderer.material.color.b + (1 - alpha / 255) * new Color32(0, 0, 0, 255).b);
        int a = (int)((alpha / 255) * renderer.material.color.a + (1 - alpha / 255) * new Color32(0, 0, 0, 255).a);

        colorz.text = "Color: RGBA ( " + r.ToString() + ", " + g.ToString() + ", " + b.ToString() + ", " + a.ToString() + " )";*/

    }

    // Update is called once per frame
    void Update()
    {

        print(Time.time);

        tiempo.text = "Time: " + Time.time.ToString();
        if (myLight.enabled == false)
        {
            luz.text = "Light: OFF";
        }
        else
        {
            luz.text = "Light: ON";
        }

        if (Time.time >=0 && Time.time <=1)
            {
                renderer.material.color = new Color32(255, 0, 0, 255);
                colorz.text = "Color: " + renderer.material.color.ToString();

        }
        else if (Time.time > 1 && Time.time < 10)
        {
            renderer.material.color = new Color32(0, 0, 0, 0);
            colorz.text = "Color: " + renderer.material.color.ToString();
        }
        if (Time.time >= 5)
        {
            myLight = lightPoint.GetComponent<Light>();
            myLight.enabled = true;

        }
        if (Time.time >= 15)
        {
            transform.Rotate(Vector3.up * (20*Time.deltaTime));

        }
        if (Time.time >= 10 && ambient > 0)
        {

            renderer.material.color = new Color32(184, 104, 152, 255);
            colorz.text = "Color: " + renderer.material.color.ToString();
            ambient = ambient - (10 * Time.deltaTime);

            renderer.material.SetFloat("_MySlider", ambient);
            
           
        }
        if (ambient < 1)
        {
            renderer.material.SetFloat("_MySlider", 1);

        }

        if (Time.time >= 15 && Time.time < 20)
        {
            shade.text = "Shader: Texture";// + renderer.material.shader.name;
            renderer.material.color = new Color32(255, 255, 255, 255);
            colorz.text = "Color: " + renderer.material.color.ToString();
            renderer.material.shader = Shader.Find("Custom/TextureShader");
            texx.text = "Texture: ON";
        }

        if (Time.time >= 20 && Time.time < 30)
        {
            shade.text = "Shader: NormalMap";
            renderer.material.color = new Color32(255,255,255,255);
            renderer.material.shader = Shader.Find("Custom/NormalMap_SinTextura");
            texx.text = "Texture: OFF";

        }


        if (Time.time >= 30 && Time.time < 60)
        {
            shade.text = "Shader: Texture + NormalMap";
            renderer.material = materials[1];
            renderer.material.color = new Color32(77, 201, 195, 255);
            colorz.text = "Color: " + renderer.material.color.ToString();
            renderer.material.shader = Shader.Find("Custom/NormalMapShader");
            texx.text = "Texture: ON";

        }

        if (Time.time >= 35 && Time.time < 55)
        {
            camera.GetComponent<Camera>().fieldOfView = Mathf.Lerp(camera.GetComponent<Camera>().fieldOfView, zoom, Time.deltaTime * smooth);
        }
    }
}

