﻿Shader "Custom/CustomShader" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_AmbientColor("AmbientColor", Color) = (1,1,1,1)
		_MySlider("SliderTarea", Range(1,10)) = 2.5
		/*_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_Glossiness ("Smoothness", Range(0,1)) = 0.5
		_Metallic ("Metallic", Range(0,1)) = 0.0*/
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf Standard fullforwardshadows

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0

		sampler2D _MainTex;

		struct Input {
			float2 uv_MainTex;
		};

		half _Glossiness;
		half _Metallic;
		fixed4 _Color;
		fixed4 _AmbientColor;
		float _MySlider;

		// Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
		// See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
		// #pragma instancing_options assumeuniformscaling
		UNITY_INSTANCING_CBUFFER_START(Props)
			// put more per-instance properties here
		UNITY_INSTANCING_CBUFFER_END


		void surf (Input IN, inout SurfaceOutputStandard o) {
			// Albedo comes from a texture tinted by color
			//fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color;
			//SinTime.w -- Time(Calcula el tiempo real), Sin (utiliza la fórmula de seno para que la función devuelva un valor que va de -1 a 1)
			//clamp para comprimir un valor y que no se salga del rango
			float rango = clamp((_SinTime.w *_MySlider), 0, _MySlider);
			fixed4 c = pow((_Color + _AmbientColor), rango);
			//fixed4 c = pow((_Color + _AmbientColor), _MySlider);
			o.Albedo = c.rgb;
			// Metallic and smoothness come from slider variables
			o.Metallic = _Metallic;
			o.Smoothness = _Glossiness;
			o.Alpha = c.a;
		}
		ENDCG
	}
	FallBack "Diffuse"
}
