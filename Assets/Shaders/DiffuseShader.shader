﻿Shader "Custom/DiffuseShader" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_AmbientColorSlider("AmbientColorProfe", Color) = (1,1,1,1)
		_MySlider("SliderExamen", Range(1,10)) = 2.5
		
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		#pragma surface surf Lambert fullforwardshadows

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0

		sampler2D _MainTex;

		struct Input {
			float2 uv_MainTex;
		};

		fixed4 _Color;
		float _MySlider;
		fixed4 _AmbientColorSlider;


		void surf (Input IN, inout SurfaceOutput o) {
			
			fixed4 c = pow((_Color + _AmbientColorSlider), _MySlider);
			o.Albedo = c.rgb;

		}
		ENDCG
	}
	FallBack "Diffuse"
}
